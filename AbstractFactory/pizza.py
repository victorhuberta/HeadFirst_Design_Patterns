from abc import ABCMeta, abstractmethod

class Pizza(metaclass=ABCMeta):
    ingredients = []

    def __init__(self, name, ingredient_factory):
        self.name = name
        self.ingredient_factory = ingredient_factory


    def prepare(self): 
        print('Preparing %s...' % self.name)
        print('Ingredients:')
        for ingredient_name in self.__class__.ingredients:
            factorymethod = getattr(self.ingredient_factory, 'create_' +
                ingredient_name)
            ingredient = factorymethod()
            setattr(self, ingredient_name, ingredient)

            if isinstance(ingredient, list):
                for item in ingredient:
                    print('    ' + type(item).__name__)
            else:
                print('    ' + type(ingredient).__name__)


    def bake(self):
        print('Bake for 25 minutes at 350')


    def cut(self):
        print('Cutting the pizza into diagonal slices')


    def box(self):
        print('Place pizza in office PizzaStore box')


class CheesePizza(Pizza):
    ingredients = ['dough', 'sauce', 'cheese']


class VeggiePizza(Pizza):
    ingredients = ['dough', 'sauce', 'cheese', 'veggies']
