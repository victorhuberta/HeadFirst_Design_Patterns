from abc import ABCMeta, abstractmethod
from pizza import *
from pizza_factory import NYPizzaIngredientFactory, \
    ChicagoPizzaIngredientFactory

class PizzaStore(metaclass=ABCMeta):

    @abstractmethod
    def __init__(self): pass


    def order_pizza(self, ptype):
        pizza = self.create_pizza(ptype)
        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()

        return pizza

    
    def create_pizza(self, ptype):
        if ptype == 'cheese':
            return CheesePizza(self.style_name + ' Cheese Pizza',
                self.ingredient_factory)
        elif ptype == 'veggie':
            return VeggiePizza(self.style_name + ' Veggie Pizza',
                self.ingredient_factory)
        elif ptype == 'clam':
            return ClamPizza(self.style_name + ' Clam Pizza',
                self.ingredient_factory)
        elif ptype == 'pepperoni':
            return PepperoniPizza(self.style_name + ' Pepperoni Pizza',
                self.ingredient_factory)
        else:
            return None


class NYPizzaStore(PizzaStore):

    def __init__(self):
        self.style_name = 'New York Style'
        self.ingredient_factory = NYPizzaIngredientFactory()


class ChicagoPizzaStore(PizzaStore):

    def __init__(self):
        self.style_name = 'Chicago Style'
        self.ingredient_factory = ChicagoPizzaIngredientFactory()
