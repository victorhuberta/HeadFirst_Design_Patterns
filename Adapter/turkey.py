from abc import ABCMeta, abstractmethod

class Turkey(metaclass=ABCMeta):

    @abstractmethod
    def gobble(self): pass


    @abstractmethod
    def fly(self): pass


class WildTurkey(Turkey):

    def gobble(self):
        print('Gobble Gobble!')


    def fly(self):
        print('I\'m flying a short distance')
