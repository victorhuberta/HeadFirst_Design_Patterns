from duck import Duck
from turkey import Turkey

class TurkeyAdapter(Duck):

    def __init__(self, turkey):
        self.turkey = turkey

    def quack(self):
        self.turkey.gobble()


    def fly(self):
        for _ in range(5):
            self.turkey.fly()
