from duck import MallardDuck
from turkey import WildTurkey
from adapter import TurkeyAdapter

def main():
    duck = MallardDuck()
    turkey = WildTurkey()
    turkey_adapter = TurkeyAdapter(turkey)
    
    test_duck(duck)
    test_duck(turkey_adapter)


def test_duck(duck):
    duck.quack()
    duck.fly()


if __name__ == '__main__':
    main()
