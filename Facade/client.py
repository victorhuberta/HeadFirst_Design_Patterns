from time import sleep
from hometheater import HomeTheaterFacade

def main():
    theater = HomeTheaterFacade.new_instance()
    theater.watch_movie('Lion King')
    sleep(2)
    theater.end_movie()


if __name__ == '__main__':
    main()
