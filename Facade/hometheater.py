from theaterdevice import TV, DVDPlayer, DVD, Volume

class HomeTheaterFacade(object):

    def __init__(self, tv, dvdplayer):
        self.tv = tv
        self.dvdplayer = dvdplayer


    @classmethod
    def new_instance(cls):
        tv = TV()
        dvdplayer = DVDPlayer()
        return cls(tv, dvdplayer)


    def watch_movie(self, movietitle):
        self.tv.on()
        self.dvdplayer.on()
        self.dvdplayer.insert_dvd(DVD(movietitle))
        self.dvdplayer.play()
        self.tv.set_volume(Volume.HIGH)
        print('Watching \'%s\' movie!' % movietitle)


    def end_movie(self):
        print('Stopped watching!')
        self.tv.set_volume(Volume.LOW)
        self.dvdplayer.stop()
        self.dvdplayer.pull_dvd()
        self.dvdplayer.off()
        self.tv.off()
