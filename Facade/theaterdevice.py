from enum import Enum

class TV(object):

    def on(self):
        print('TV turned ON.')


    def off(self):
        print('TV turned OFF.')


    def set_volume(self, volume):
        print('TV volume set to %s.' % volume)


class Volume(Enum):
    LOW = 0
    MEDIUM = 1
    HIGH = 2


class DVDPlayer(object):

    def on(self):
        print('DVD player turned ON.')


    def off(self):
        print('DVD player turned OFF.')


    def insert_dvd(self, dvd):
        print('Inserting \'%s\' DVD...' % dvd.title)
        self.dvd = dvd
    

    def pull_dvd(self):
        print('Pulling out \'%s\' DVD...' % self.dvd.title)
        self.dvd = None


    def play(self):
        print('Playing \'%s\' DVD...' % self.dvd.title)


    def stop(self):
        print('Stopping \'%s\' DVD...' % self.dvd.title)


class DVD(object):

    def __init__(self, title):
        self.title = title
