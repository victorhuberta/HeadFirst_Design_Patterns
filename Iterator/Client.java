import java.util.Map;
import java.util.HashMap;

public class Client {

    public static void main(String[] args) {
        Map<String, Menu> allMenu = new HashMap<>();
        allMenu.put("BREAKFAST", new PancakeHouseMenu());
        allMenu.put("LUNCH", new DinerMenu());
        allMenu.put("DINNER", new CafeMenu());

        Waitress waitress = new Waitress(allMenu);
        waitress.printAllMenu();
    }

}
