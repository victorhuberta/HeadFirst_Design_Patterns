import java.util.Iterator;

public class DinerMenu implements Menu {

    private final int MAX_ITEMS = 6;
    private MenuItem[] menuItems;
    private int nMenuItems = 0;

    public DinerMenu() {
        menuItems = new MenuItem[MAX_ITEMS];

        try {
            addMenuItem("Vegetarian BLT",
                "(Fakin') Bacon with lettuce & tomato on whole wheat",
                2.99);
            addMenuItem("BLT",
                "Bacon with lettuce & tomato on whole wheat",
                2.99);
            addMenuItem("Soup of the day",
                "Soup of the day, with a side of potato salad",
                3.29);
            addMenuItem("Hotdog",
                "A hot dog, with saurkraut, relish, onions, topped with cheese",
                3.05);
        } catch (MenuException e) {
            e.printStackTrace();
        }
    }

    public void addMenuItem(String name, String description, double price)
        throws MenuException
    {

        if (nMenuItems >= MAX_ITEMS) throw new MenuException("Max items reached");

        MenuItem menuItem = new DinerMenuItem(name, description, price);
        menuItems[nMenuItems++] = menuItem;
    }

    public Iterator createIterator() {
        return new DinerMenuIterator(menuItems);
    }

}
