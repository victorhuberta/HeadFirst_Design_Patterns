import java.util.Map;
import java.util.Iterator;

public class Waitress {

    private Map<String, Menu> allMenu;

    public Waitress(Map<String, Menu> allMenu) {
        this.allMenu = allMenu;
    }

    public void printAllMenu() {
        System.out.println("MENU\n----");
        for (Map.Entry<String, Menu> menu: allMenu.entrySet()) {
            System.out.println("\n" + menu.getKey().toUpperCase());
            printMenu(menu.getValue());
        }
    }

    public void printMenu(Menu menu) {
        Iterator menuIterator = menu.createIterator();

        while (menuIterator.hasNext()) {
            MenuItem menuItem = (MenuItem) menuIterator.next();
            System.out.print(menuItem.getName() + ", ");
            System.out.print(menuItem.getPrice() + " -- ");
            System.out.println(menuItem.getDescription());
        }
    }

}
