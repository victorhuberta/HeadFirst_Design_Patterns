import java.util.*;

public class CafeMenu implements Menu {

    private Map<String, MenuItem> menuItems;

    public CafeMenu() {
        menuItems = new HashMap<>();

        try {
            addMenuItem("Veggie Burger and Air Fries",
                "Veggie burger on a whole wheat bun, lettuce, tomato, and fries",
                3.99);
            addMenuItem("Soup of the day",
                "A cup of the soup of the day, with a side salad",
                3.69);
            addMenuItem("Burrito",
                "A large burrito, with a whole pinto beans, salsa, guacamole",
                4.29);
        } catch (MenuException e) {
            e.printStackTrace();
        }
    }

    public void addMenuItem(String name, String description, double price)
        throws MenuException
    {
        MenuItem menuItem = new CafeMenuItem(name, description, price);
        menuItems.put(name, menuItem);
    }

    public Iterator createIterator() {
        return menuItems.values().iterator();
    }

}
