import java.util.*;

public class PancakeHouseMenu implements Menu {

    private ArrayList<MenuItem> menuItems;

    public PancakeHouseMenu() {
        menuItems = new ArrayList<>();

        try {
            addMenuItem("K&B's Pancake Breakfast",
                "Pancakes with scrambled eggs, and toast",
                2.99);
            addMenuItem("Regular Pancake Breakfast",
                "Pancakes with fried eggs, sausage",
                2.99);
            addMenuItem("Blueberry Pancakes",
                "Pancakes made with fresh blueberries",
                3.49);
            addMenuItem("Waffles",
                "Waffles, with your choice of blueberries or strawberries",
                3.59);
        } catch (MenuException e) {
            e.printStackTrace();
        }
    }

    public Iterator createIterator() {
        return menuItems.iterator();
    }

    public void addMenuItem(String name, String description, double price)
        throws MenuException
    {
        MenuItem menuItem = new PancakeHouseMenuItem(name, description, price);
        menuItems.add(menuItem);
    }
}
