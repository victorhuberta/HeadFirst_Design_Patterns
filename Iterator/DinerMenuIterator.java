import java.util.Iterator;

public class DinerMenuIterator implements Iterator {

    private MenuItem[] menuItems;
    private int currentIndex = 0;

    public DinerMenuIterator(MenuItem[] menuItems) {
        this.menuItems = menuItems;
    }

    public boolean hasNext() {
        return (currentIndex < menuItems.length)
            && (menuItems[currentIndex] != null);
    }

    public Object next() {
        return menuItems[currentIndex++];
    }

    public void remove() {
        if (currentIndex == 0)
            throw new IllegalStateException("Call next() first, bitch");

        for (int i = currentIndex - 1;
            i < (menuItems.length - 1);
            ++i)
        {
            menuItems[i] = menuItems[i+1];
        }
        menuItems[menuItems.length - 1] = null;
    }

}
