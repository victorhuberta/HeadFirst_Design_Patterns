import java.util.Iterator;

public interface Menu {

    public void addMenuItem(
        String name, String description, double price
    ) throws MenuException;

    public Iterator createIterator();

}
