from abc import ABCMeta, abstractmethod
from functools import partial, wraps

class SubjectDescriptor(object):
    
    def __init__(self, name):
        self.name = name


    def __set__(self, observer, new_subject):
        old_subject = observer.__dict__.get(self.name)
        if old_subject is not None:
            old_subject.remove_observer(observer)
        new_subject.register_observer(observer)
        observer.__dict__[self.name] = new_subject


    def __delete__(self, observer):
        del(observer.__dict__[self.name])


class Observer(metaclass=ABCMeta):
    subject = SubjectDescriptor('subject')

    def __init__(self, subject):
        self.subject = subject

    @abstractmethod
    def update(self, data): pass


class DisplayElement(metaclass=ABCMeta):

    @abstractmethod
    def display(self): pass


def expectdata(func=None, *, has):
    if not callable(func):
        return partial(expectdata, has=has)

    data_expectations = has

    @wraps(func)
    def update_with_expectations(self, data, *args):
        for expectation in data_expectations:
            try:
                data.__dict__[expectation]
            except KeyError:
                return
        func(self, data, *args)

    return update_with_expectations


class WeatherConditionsDisplay(Observer, DisplayElement):

    def __init__(self, subject):
        super().__init__(subject)
        self.temperature = 0
        self.humidity = 0
        self.pressure = 0


    @expectdata(has=['temperature', 'humidity', 'pressure'])
    def update(self, data):
        self.temperature = data.temperature
        self.humidity = data.humidity
        self.pressure = data.pressure


    def display(self):
        print('Weather Conditions:')
        print('* Temperature: %d C'% self.temperature)
        print('* Humidity: %d%%' % self.humidity)
        print('* Pressure: %d%%' % self.pressure)


class HeatIndexDisplay(Observer, DisplayElement):

    def __init__(self, subject):
        super().__init__(subject)
        self.heat_index = 0


    @expectdata(has=['temperature', 'humidity'])
    def update(self, data):
        self.heatindex = self.calc_heatindex(
            data.temperature, data.humidity)


    def display(self):
        print('Heat index is', self.heatindex)


    def calc_heatindex(self, temperature, humidity):
        t, h = temperature, humidity
        heatindex = 16.923 + 1.85212*1/10*t + 5.37941*h - \
            1.00254*1/10*t*h + 9.41695*1/1000*pow(t, 2) + \
            7.28898*1/1000*h + 3.45372*1/10000*pow(t, 2)*h

        return heatindex
