from abc import ABCMeta, abstractmethod

class Subject(metaclass=ABCMeta):

    @abstractmethod
    def register_observer(self, observer): pass

    @abstractmethod
    def remove_observer(self, observer): pass

    @abstractmethod
    def notify_observers(self, data): pass


class WeatherSubject(Subject):

    def __init__(self):
        self.observers = []


    def register_observer(self, observer):
        self.observers.append(observer)


    def remove_observer(self, observer):
        self.observers.remove(observer)


    def notify_observers(self, data):
        for observer in self.observers:
            observer.update(data)
