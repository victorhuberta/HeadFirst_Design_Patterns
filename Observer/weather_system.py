from time import sleep
from weather_station import WeatherDataGatherer
from weather_server import WeatherSubject
from weather_client import WeatherConditionsDisplay, HeatIndexDisplay

def main():
    weather_updater = WeatherSubject()
    conditions_display = WeatherConditionsDisplay(weather_updater)
    heatindex_display = HeatIndexDisplay(weather_updater)
    wdg = WeatherDataGatherer(weather_updater)

    while True:
        wdg.getdata_temperature()
        wdg.getdata_humidity()
        wdg.getdata_pressure()
        sleep(1)
        conditions_display.display()
        heatindex_display.display()


if __name__ == '__main__':
    main()
