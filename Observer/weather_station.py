import random
from inspect import isclass
from functools import partial, wraps

class WeatherData:

    def __init__(self, callback):
        self.callback = callback


def updatedata(func=None, *, dataname='data'):
    if not callable(func):
        return partial(updatedata, dataname=dataname)

    @wraps(func)
    def wrapped_getter(self, *args):
        data = self.__dict__[dataname]
        attr = func.__name__.split('getdata_').pop()
        val = func(self, *args)

        attr_exists = hasattr(data, attr)

        if attr_exists:
            prev_val = getattr(data, attr)
            state_changed = True if prev_val != val else False

        setattr(data, attr, val)

        # Only run callback on either new attribute or state change.
        if not attr_exists or state_changed:
            data.callback()

    return wrapped_getter


def auto_updatedata(cls=None, *, dataname='data'):
    if not isclass(cls):
        return partial(auto_updatedata, dataname=dataname)

    for attr, val in cls.__dict__.items():
        if callable(val) and attr.startswith('getdata_'):
            setattr(cls, attr, updatedata(val, dataname=dataname))

    return cls


@auto_updatedata(dataname='data')
class WeatherDataGatherer:

    def __init__(self, weather_updater):
        self.weather_updater = weather_updater
        self.data = WeatherData(self.measurements_changed)


    def getdata_temperature(self):
        """Get temperature in Celcius."""
        return random.randint(-10, 30)
    

    def getdata_humidity(self):
        """Get humidity level (0% - 100%)."""
        return random.randint(0, 100)


    def getdata_pressure(self):
        """Get barometric pressure level (0% - 100%)."""
        return random.randint(0, 100)


    def measurements_changed(self):
        self.weather_updater.notify_observers(self.data)
