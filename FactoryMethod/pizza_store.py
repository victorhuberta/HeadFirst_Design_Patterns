from abc import ABCMeta, abstractmethod
from pizza import *

class PizzaStore(metaclass=ABCMeta):

    def order_pizza(self, ptype):
        pizza = self.create_pizza(ptype)
        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()

        return pizza


    @abstractmethod
    def create_pizza(self, ptype): pass


class NYPizzaStore(PizzaStore):

    def create_pizza(self, ptype):
        if ptype == 'cheese':
            return NYStyleCheesePizza()
        elif ptype == 'veggie':
            return NYStyleVeggiePizza()
        elif ptype == 'clam':
            return NYStyleClamPizza()
        elif ptype == 'pepperoni':
            return NYStylePepperoniPizza()
        else:
            return None


class ChicagoPizzaStore(PizzaStore):

    def create_pizza(self, ptype):
        if ptype == 'cheese':
            return ChicagoStyleCheesePizza()
        elif ptype == 'veggie':
            return ChicagoStyleVeggiePizza()
        elif ptype == 'clam':
            return ChicagoStyleClamPizza()
        elif ptype == 'pepperoni':
            return ChicagoStylePepperoniPizza()
        else:
            return None
