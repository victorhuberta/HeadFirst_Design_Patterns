from abc import ABCMeta, abstractmethod

class Pizza(metaclass=ABCMeta):

    @abstractmethod
    def __init__(self):
        self.name = ''
        self.dough = ''
        self.sauce = ''
        self.toppings = []


    def prepare(self):
        print('Preparing', self.name)
        print('Tossing dough...')
        print('Adding sauce...')
        print('Adding toppings:')
        for topping in self.toppings:
            print('    %s' % topping)


    def bake(self):
        print('Bake for 25 minutes at 350')


    def cut(self):
        print('Cutting the pizza into diagonal slices')


    def box(self):
        print('Place pizza in office PizzaStore box')


class NYStyleCheesePizza(Pizza):

    def __init__(self):
        super().__init__()

        self.name = 'NY Style Sauce and Cheese Pizza'
        self.dough = 'Thin Crust Dough'
        self.sauce = 'Marinara Sauce'

        self.toppings.append('Grated Reggiano Cheese')


class ChicagoStyleCheesePizza(Pizza):

    def __init__(self):
        super().__init__()

        self.name = 'Chicago Style Deep Dish Cheese Pizza'
        self.dough = 'Extra Thick Crust Dough'
        self.sauce = 'Plum Tomato Sauce'
        
        self.toppings.append('Shredded Mozzarella Cheese')


    def cut(self):
        print('Cutting the pizza into square slices')
