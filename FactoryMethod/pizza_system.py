from pizza_store import NYPizzaStore, ChicagoPizzaStore

def main():
    nystore = NYPizzaStore()
    pizza = nystore.order_pizza('cheese')
    print('Ethan ordered a', pizza.name)

    cstore = ChicagoPizzaStore()
    pizza = cstore.order_pizza('cheese')
    print('Joel ordered a', pizza.name)


if __name__ == '__main__':
    main()
