public class GumballMachine implements Context<GMachineState> {
    private GMachineState soldOutState;
    private GMachineState noQuarterState;
    private GMachineState hasQuarterState;
    private GMachineState soldState;
    private GMachineState currentState;

    private int ballCount;

    public GumballMachine(int ballCount) {
        soldOutState = new SoldOutState(this);
        noQuarterState = new NoQuarterState(this);
        hasQuarterState = new HasQuarterState(this);
        soldState = new SoldState(this);

        this.ballCount = ballCount;

        currentState = (ballCount > 0) ? noQuarterState : soldOutState;
    }

    public GMachineState getState() {
        return currentState;
    }

    public void setState(GMachineState newState) {
        currentState = newState;
    }

    public void insertQuarter() {
        if (currentState.insertQuarter() == ActionResult.SUCCESS) {
            setState(hasQuarterState);
        }
    }

    public void ejectQuarter() {
        if (currentState.ejectQuarter() == ActionResult.SUCCESS) {
            setState(noQuarterState);
        }
    }

    public void turnCrank() {
        if (currentState.turnCrank() == ActionResult.SUCCESS) {
            setState(soldState);

            if (currentState.dispense() == ActionResult.SUCCESS) {
                ballCount -= 1;
                if (ballCount == 0) {
                    System.out.println("Oops, out of gumballs!");
                    setState(soldOutState);
                } else {
                    setState(noQuarterState);
                }
            }
        }
    } 

    public void refill(int ballCount) {
        assert ballCount >= 0;

        this.ballCount += ballCount;
        currentState = (this.ballCount > 0) ? noQuarterState : soldOutState;
    }

    public String toString() {
        return "\nMighty Gumball, Inc.\n" +
            "Java-enabled Standing Gumball Model #2004\n" +
            "Inventory: " + ballCount + " gumballs\n" +
            ((currentState == noQuarterState) ?
                "Machine is waiting for quarter" :
                ((currentState == hasQuarterState) ?
                    "You can turn the crank now" :
                    ((currentState == soldState) ?
                        "Machine is ready to dispense" :
                        ((currentState == soldOutState) ?
                            "Machine is sold out" : "")))) +
            "\n";
    }

}
