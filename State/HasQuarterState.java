public class HasQuarterState implements GMachineState {
    
    private Context context;

    public HasQuarterState(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ActionResult insertQuarter() {
        System.out.println("You can't insert another quarter");
        return ActionResult.SUCCESS;
    }

    public ActionResult ejectQuarter() {
        System.out.println("Quarter returned");
        return ActionResult.SUCCESS;
    }

    public ActionResult turnCrank() {
        System.out.println("You turned...");
        return ActionResult.SUCCESS;
    }

    public ActionResult dispense() {
        System.out.println("No gumball dispensed");
        return ActionResult.FAILURE;
    }

}
