public interface Context<T> {
    
    public T getState();

    public void setState(T newState);

}
