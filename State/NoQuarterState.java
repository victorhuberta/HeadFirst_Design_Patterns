public class NoQuarterState implements GMachineState {
    
    private Context context;

    public NoQuarterState(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ActionResult insertQuarter() {
        System.out.println("You inserted a quarter");
        return ActionResult.SUCCESS;
    }

    public ActionResult ejectQuarter() {
        System.out.println("You haven't inserted a quarter");
        return ActionResult.FAILURE;
    }

    public ActionResult turnCrank() {
        System.out.println("You turned, but there's no quarter");
        return ActionResult.FAILURE;
    }

    public ActionResult dispense() {
        System.out.println("You need to pay first");
        return ActionResult.FAILURE;
    }

}
