public class SoldState implements GMachineState {
    
    private Context context;

    public SoldState(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ActionResult insertQuarter() {
        System.out.println("Please wait, we're already giving you a gumball");
        return ActionResult.FAILURE;
    }

    public ActionResult ejectQuarter() {
        System.out.println("Sorry, you already turned the crank");
        return ActionResult.FAILURE;
    }

    public ActionResult turnCrank() {
        System.out.println("Turning twice doesn't get you another gumball!");
        return ActionResult.FAILURE;
    }

    public ActionResult dispense() {
        System.out.println("A gumball comes rolling out the slot");
        return ActionResult.SUCCESS;
    }

}
