public class SoldOutState implements GMachineState {
    
    private Context context;

    public SoldOutState(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ActionResult insertQuarter() {
        System.out.println("You can't insert a " +
            "quarter, the machine is sold out");
        return ActionResult.FAILURE;
    }

    public ActionResult ejectQuarter() {
        System.out.println("You can't eject, you " +
            "haven't inserted a quarter yet");
        return ActionResult.FAILURE;
    }

    public ActionResult turnCrank() {
        System.out.println("You turned, but there are no gumballs");
        return ActionResult.FAILURE;
    }

    public ActionResult dispense() {
        System.out.println("No gumball dispensed");
        return ActionResult.FAILURE;
    }

}
