public interface State<T> {

    public T getContext();

    public void setContext(T newContext);

}
