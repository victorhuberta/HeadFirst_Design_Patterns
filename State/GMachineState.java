public interface GMachineState extends State<Context> {

    public ActionResult insertQuarter();

    public ActionResult ejectQuarter();

    public ActionResult turnCrank();

    public ActionResult dispense();

}
