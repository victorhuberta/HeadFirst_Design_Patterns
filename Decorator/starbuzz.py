from beverage import Espresso, DarkRoast, HouseBlend, BeverageSize
from condiment import Mocha, Whip, Soy

def main():
    beverage = Espresso(BeverageSize.grande)
    print(beverage.description(), '$' + str(beverage.cost()))

    beverage2 = Whip(Mocha(Mocha(DarkRoast(BeverageSize.tall))))
    print(beverage2.description(), '$' + str(beverage2.cost()))

    beverage3 = Whip(Mocha(Soy(HouseBlend(BeverageSize.venti))))
    print(beverage3.description(), '$' + str(beverage3.cost()))


if __name__ == '__main__':
    main()
