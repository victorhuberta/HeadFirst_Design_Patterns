from abc import ABCMeta, abstractmethod
from enum import Enum

class Item(metaclass=ABCMeta):

    @abstractmethod
    def description(self): pass


    @abstractmethod
    def cost(self): pass


class BeverageSize(Enum):
    tall = 1
    grande = 2
    venti = 3


size_prices = {
    BeverageSize.tall: 0.10,
    BeverageSize.grande: 0.15,
    BeverageSize.venti: 0.20,
}


class Beverage(Item):

    def __init__(self, size):
        self.size = size


    @abstractmethod
    def description(self): pass


    @abstractmethod
    def cost(self): pass


class HouseBlend(Beverage):

    def description(self):
        return 'House Blend Coffee'


    def cost(self):
        return 0.89
    

class DarkRoast(Beverage):

    def description(self):
        return 'Dark Roast Coffee'


    def cost(self):
        return 0.99


class Espresso(Beverage):

    def description(self):
        return 'Espresso'


    def cost(self):
        return 1.99


class Decaf(Beverage):

    def description(self):
        return 'Decaf'


    def cost(self):
        return 1.05
