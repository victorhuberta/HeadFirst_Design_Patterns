from abc import abstractmethod
from beverage import Item, size_prices

class CondimentDecorator(Item):

    def __init__(self, beverage):
        self.beverage = beverage
        self.size = beverage.size


    @abstractmethod
    def description(self): pass


    @abstractmethod
    def cost(self): pass


class Mocha(CondimentDecorator):

    def description(self):
        return self.beverage.description() + ', Mocha'


    def cost(self):
        return .20 + size_prices[self.size] + self.beverage.cost()


class Milk(CondimentDecorator):
    
    def description(self):
        return self.beverage.description() + ', Milk'


    def cost(self):
        return .10 + size_prices[self.size] + self.beverage.cost()


class Soy(CondimentDecorator):

    def description(self):
        return self.beverage.description() + ', Soy'


    def cost(self):
        return .15 + size_prices[self.size] + self.beverage.cost()


class Whip(CondimentDecorator):

    def description(self):
        return self.beverage.description() + ', Whip'


    def cost(self):
        return .10 + size_prices[self.size] + self.beverage.cost()
