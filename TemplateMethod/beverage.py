from abc import ABCMeta, abstractmethod

class CaffeineBeverage(metaclass=ABCMeta):

    def prepare_recipe(self):
        self.boil_water()
        self.brew()
        self.pour_in_cup()
        self.add_secret_powder()
        self.add_condiments()


    def boil_water(self):
        print('Boiling water...')
    

    def pour_in_cup(self):
        print('Pouring in cup...')


    def add_secret_powder(self):
        """This is a hook method.
        Subclasses can optionally override it to hook themselves
        into the computation. It does nothing otherwise.
        """
        pass


    @abstractmethod
    def brew(self): pass


    @abstractmethod
    def add_condiments(self): pass


class Coffee(CaffeineBeverage):

    def brew(self):
        print('Brewing the coffee grinds...')


    def add_condiments(self):
        print('Adding sugar and milk...')


    def add_secret_powder(self):
        print('Adding secret KungPow powder...')


class Tea(CaffeineBeverage):

    def brew(self):
        print('Steeping the teabag in the water...')


    def add_condiments(self):
        print('Adding lemon...')
