from beverage import Coffee, Tea

def main():
    drink = Coffee()
    drink.prepare_recipe()

    drink = Tea()
    drink.prepare_recipe()


if __name__ == '__main__':
    main()
