class ChocolateBoiler {
    
    private static ChocolateBoiler uniqueInstance;
    private boolean isEmpty;
    private boolean isBoiled;

    private ChocolateBoiler() {
        isEmpty = true;
        isBoiled = false;
    }

    public static ChocolateBoiler getInstance() {
        // Alternatives to make this implementation multi-thread-friendly are:
        // 1. Use eager initialization rather than a lazy one.
        //     i.e. private ... uniqueInstance = new ChocolateBoiler();
        // 2. Synchronize getInstance().
        // 3. Only synchronize the first time instance is created.
        if (uniqueInstance == null) {
            synchronized (ChocolateBoiler.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new ChocolateBoiler();
                }
            }
        }
        return uniqueInstance;
    }

    public void fill() {
        if (isEmpty) {
            isEmpty = false;
            isBoiled = false;
        }
    }

    public void drain() {
        if (! isEmpty && isBoiled) {
            isEmpty = true;
        }
    }

    public void boil() {
        if (! isEmpty && ! isBoiled) {
            isBoiled = true;
        }
    }

}

public class ChocolateBoilerTest {
    
    public static void main(String[] args) {
        ChocolateBoiler boiler = ChocolateBoiler.getInstance();
        boiler.fill();
        boiler.boil();
        boiler.drain();
    }

}
