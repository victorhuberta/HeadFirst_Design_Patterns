import java.util.*;

public class Menu extends MenuComponent {

    private String name;
    private String description;
    private ArrayList<MenuComponent> children = new ArrayList<>();;

    public Menu(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void addComponent(MenuComponent menuComponent) {
        children.add(menuComponent);
    }

    public void removeComponent(MenuComponent menuComponent) {
        children.remove(menuComponent);
    }

    public MenuComponent getChildComponent(int i) {
        return children.get(i);
    }

    public Iterator createIterator() {
        return new CompositeIterator(children.iterator());
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void print() {
        System.out.print("\n" + getName());
        System.out.println(", " + getDescription());
        System.out.println("---------------------"); 

        Iterator iterator = children.iterator();
        while (iterator.hasNext()) {
            MenuComponent menuComponent = (MenuComponent) iterator.next();
            menuComponent.print();
        }
    }

}
