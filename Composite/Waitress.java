import java.util.Iterator;

public class Waitress {

    private MenuComponent allMenus;

    public Waitress(MenuComponent allMenus) {
        this.allMenus = allMenus;
    }

    public void printAllMenus() {
        allMenus.print();
    }

    public void printMenusWithPriceLimit(double priceLimit) {
        System.out.println("\nMENU UNDER PRICE LIMIT: " + priceLimit);
        System.out.println("---------------------");

        Iterator iterator = allMenus.createIterator();
        while (iterator.hasNext()) {
            MenuComponent menuComponent = (MenuComponent) iterator.next();
            try {
                double price = menuComponent.getPrice();
                if (price <= priceLimit) {
                    menuComponent.print();
                }
            } catch (UnsupportedOperationException e) {}
        }
    }

}
