public class MenuTestDrive {
    public static void main(String args[]) {
        MenuComponent pancakeHouseMenu =
            new Menu("PANCAKE HOUSE MENU", "Breakfast");
        pancakeHouseMenu.addComponent(new MenuItem(
            "Regular Pancake Breakfast",
            "Pancakes with fried eggs, sausage",
            2.99));
        pancakeHouseMenu.addComponent(new MenuItem(
            "Waffles",
            "Waffles, with your choice of blueberries or strawberries",
            3.59));

        MenuComponent dinerMenu =
            new Menu("DINER MENU", "Lunch");
        dinerMenu.addComponent(new MenuItem(
            "Hotdog",
            "A hot dog, with saurkraut, relish, onions, topped with cheese",
            3.05));
        dinerMenu.addComponent(new MenuItem(
            "Pasta",
            "Spaghetti with Marinara Sauce, and a slice of sourdough bread",
            3.89));

        MenuComponent dessertMenu =
            new Menu("DESSERT MENU", "Dessert of course!");
        dessertMenu.addComponent(new MenuItem(
            "Apple Pie",
            "Apple pie with a flakey crust, topped with vanilla icecream",
            1.59));

        MenuComponent cafeMenu =
            new Menu("CAFE MENU", "Dinner");
        cafeMenu.addComponent(new MenuItem(
            "Soup of the day",
            "A cup of the soup of the day, with a side salad",
            3.69));
        cafeMenu.addComponent(new MenuItem(
            "Burrito",
            "A large burrito, with whole pinto beans, salsa, guacamole",
            4.29));

        MenuComponent allMenus = new Menu("ALL MENUS", "All menus combined");
        allMenus.addComponent(pancakeHouseMenu);
        allMenus.addComponent(dinerMenu);
        allMenus.addComponent(cafeMenu);

        dinerMenu.addComponent(dessertMenu);

        Waitress w = new Waitress(allMenus);
        w.printAllMenus();
        w.printMenusWithPriceLimit(3.0);
    }
}
