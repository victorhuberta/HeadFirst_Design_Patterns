import java.util.Iterator;

public abstract class MenuComponent implements Component<MenuComponent> {

    public void addComponent(MenuComponent menuComponent) {
        throw new UnsupportedOperationException();
    }

    public void removeComponent(MenuComponent menuComponent) {
        throw new UnsupportedOperationException();
    }

    public MenuComponent getChildComponent(int i) {
        throw new UnsupportedOperationException();
    }

    public Iterator createIterator() {
        throw new UnsupportedOperationException();
    }

    public String getName() {
        throw new UnsupportedOperationException();
    }

    public String getDescription() {
        throw new UnsupportedOperationException();
    }

    public double getPrice() {
        throw new UnsupportedOperationException();
    }

    public void print() {
        throw new UnsupportedOperationException();
    }

}
