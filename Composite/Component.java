import java.util.Iterator;

public interface Component<T> {

    public void addComponent(T component);
    public void removeComponent(T component);
    public T getChildComponent(int i); 
    public Iterator createIterator();

}
