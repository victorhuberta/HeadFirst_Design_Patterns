from duck import ModelDuck
from flybehavior import FlyRocketPowered

def main():
    modelduck = ModelDuck()
    modelduck.perform_quack()
    modelduck.perform_fly()
    modelduck.set_flybehavior(FlyRocketPowered())
    modelduck.perform_fly()


if __name__ == '__main__':
    main()
