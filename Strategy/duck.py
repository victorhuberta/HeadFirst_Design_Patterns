from abc import ABCMeta, abstractmethod
from flybehavior import FlyNoWay
from quackbehavior import Quack

class Duck(metaclass=ABCMeta):
    
    def __init__(self):
        self.flybehavior = FlyNoWay()
        self.quackbehavior = Quack()


    @abstractmethod
    def display(self):
        pass


    def swim(self):
        print('All ducks float, even decoys!')


    def perform_fly(self):
        self.flybehavior.fly()


    def perform_quack(self):
        self.quackbehavior.quack()


    def set_flybehavior(self, flybehavior):
        self.flybehavior = flybehavior


    def set_quackbehavior(self, quackbehavior):
        self.quackbehavior = quackbehavior


class ModelDuck(Duck):

    def __init__(self):
        self.flybehavior = FlyNoWay()
        self.quackbehavior = Quack()


    def display(self):
        print('I\'m a model duck')
