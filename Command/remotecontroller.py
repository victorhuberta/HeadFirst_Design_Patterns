from command import NoCommand

class RemoteController(object):

    def __init__(self, n_slots):
        self.n_slots = n_slots
        self.commands = [{
            'on': NoCommand(),
            'off': NoCommand()
        } for _ in range(self.n_slots)]
        self.undo_command = NoCommand()


    def set_command(self, slot, cmdname, command):
        self._check_bounds(slot)
        self.commands[slot][cmdname.lower()] = command


    def button_pressed(self, slot, cmdname, *args):
        self._check_bounds(slot)
        cmd = self.commands[slot][cmdname.lower()]
        cmd.execute(*args)
        self.undo_command = cmd


    def _check_bounds(self, slot):
        if slot < 0 or slot >= len(self.commands):
            raise IndexError('There are only %d commands.' % self.n_slots)


    def undo_button_pressed(self):
        print('Undoing %s...' % self.undo_command)
        self.undo_command.undo()


    def __str__(self):
        output = ''
        for i in range(len(self.commands)):
            slot = self.commands[i]
            output += '[slot %d]:' % i
            for cmdname, command in slot.items():
                output += (' | %s -> %s' % (cmdname.upper(),
                    command.__str__()))
            output += '\n'

        return output


