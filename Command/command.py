from abc import ABCMeta, abstractmethod
from functools import wraps

class Command(metaclass=ABCMeta):

    @abstractmethod
    def execute(self, *args): pass

    
    @abstractmethod
    def undo(self): pass


def ifexecuted(func):
    @wraps(func)
    def wrapper(self, *args):
        if self.is_executed:
            func(self, *args)
            self.is_executed = False

    return wrapper


def markexecuted(func):
    @wraps(func)
    def wrapper(self, *args):
        func(self, *args)
        self.is_executed = True

    return wrapper


class NoCommand(Command):

    def execute(self, *args): pass


    def undo(self): pass


    def __str__(self):
        return 'NoCommand'
    

class LightCommand(Command):

    def __init__(self, light):
        self.light = light
        self.is_executed = False


    def execute(self, *args): pass
    
    
    def undo(self): pass


    def __str__(self):
        return self.__class__.__name__ + \
            '(' + self.light.__class__.__name__ + ')'


class LightOnCommand(LightCommand):

    @markexecuted
    def execute(self, *args):
        self.light.on()

    
    @ifexecuted
    def undo(self):
        self.light.off()


class LightOffCommand(LightCommand):
    
    @markexecuted
    def execute(self, *args):
        self.light.off()


    @ifexecuted
    def undo(self):
        self.light.on()


class LightCalibrateCommand(LightCommand):

    @markexecuted
    def execute(self, *args):
        self.prev_brightness = self.light.brightness
        self.light.calibrate(*args)


    @ifexecuted
    def undo(self):
        self.light.calibrate(self.prev_brightness)


class MacroCommand(Command):

    def __init__(self, commands):
        self.commands = commands

    
    def execute(self, *args):
        for command in self.commands: command.execute(*args)


    def undo(self):
        for command in self.commands[::-1]: command.undo()


    def __str__(self):
        return 'MacroCommand' + str([str(cmd) for cmd in self.commands])
