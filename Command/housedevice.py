from abc import ABCMeta, abstractmethod

class Light(metaclass=ABCMeta):

    @abstractmethod
    def on(self): pass

    
    @abstractmethod
    def off(self): pass


class AdjustableBrightness(metaclass=ABCMeta):

    @abstractmethod
    def calibrate(self, level): pass


class LivingRoomLight(Light):

    def on(self):
        print('Living room light turned on.')


    def off(self):
        print('Living room light turned off.')


class BedroomLight(Light, AdjustableBrightness):

    def __init__(self):
        self.brightness = 0


    def on(self):
        print('Bedroom light turned on.')


    def off(self):
        print('Bedroom light turned off.')


    def calibrate(self, brightness):
        self.brightness = brightness
        print('Bedroom light\'s brightness calibrated to %d%%.' % brightness)
