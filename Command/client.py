from remotecontroller import RemoteController
from housedevice import LivingRoomLight, BedroomLight
from command import *

def main():
    remote = RemoteController(7)
    livingroom_light = LivingRoomLight()
    bedroom_light = BedroomLight()

    light_on_cmd = LightOnCommand(livingroom_light)
    light_off_cmd = LightOffCommand(livingroom_light)

    remote.set_command(0, 'on', light_on_cmd)
    remote.button_pressed(0, 'on')
    remote.undo_button_pressed()

    remote.set_command(1, 'off', light_off_cmd)
    remote.button_pressed(1, 'off')
    remote.undo_button_pressed()

    light_on_cmd = LightOnCommand(bedroom_light)
    light_off_cmd = LightOffCommand(bedroom_light)
    light_cal_cmd = LightCalibrateCommand(bedroom_light)

    remote.set_command(2, 'on', light_on_cmd)
    remote.button_pressed(2, 'on')
    remote.undo_button_pressed()

    remote.set_command(3, 'off', light_off_cmd)
    remote.button_pressed(3, 'off')
    remote.undo_button_pressed()

    remote.set_command(4, 'cal', light_cal_cmd)
    remote.button_pressed(4, 'cal', 65)
    remote.undo_button_pressed()

    partyon_cmd = MacroCommand([light_on_cmd, light_cal_cmd])
    remote.set_command(6, 'partyon', partyon_cmd)
    remote.button_pressed(6, 'partyon', 100)
    remote.undo_button_pressed()

    print(remote)


if __name__ == '__main__':
    main()
